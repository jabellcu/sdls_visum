import os
import glob

#Loops through the Link Bar clases, activating only the first one, then
#changing the scale attribute for each matrix.

# version name
verp = Visum.UserPreferences.DocumentName
verfl, verfn = os.path.split(verp)
vern, _ = os.path.splitext(verfn)

ofldr = 'screenshots'

# make sure export directory exists
if not os.path.isdir(ofldr):
    os.mkdir(ofldr)

sgpas = glob.glob('*.gpa')

# filter matrices:
def isinteresting():
    return True

# No for Visum.Net.Matrices.ItemByKey(No)
tmats = {}
for mat in Visum.Net.Matrices:
    no = mat.AttValue("No")
    code = mat.AttValue("Code")
    if isinteresting():
        tmats[no] = code

gpa_lyr = Visum.Net.GraphicParameters.LinkGroup
gpa_lyr.SetAttValue("DRAW", True)  # Activate lyr

for sgpa in sgpas:
    sgpan, _ = os.path.splitext(sgpa)
    Visum.Net.GraphicParameters.Open(sgpa)
    for no, code in tmats.items():

        att = 'VOLVEH_DSEG({},AP)'.format(code)

        # Activate only first bar, set DSeg to mat code
        itr = gpa_lyr.BarIterator
        firstbar = itr.Item
        firstbar.SetAttValue('DRAW', True)
        firstbar.SetAttValue('ScaleAttrID', att)
        itr.Next()
        while itr.Valid:
            bar = itr.Item
            bar.SetAttValue('DRAW', False)
            itr.Next()
        itr.Reset()

        Visum.Graphic.Draw()  # refresh

        ofn = '{}_{}.jpg'.format(sgpan, code)
        ofp = os.path.join(ofldr, ofn)
        resolution = 10
        Visum.Graphic.Screenshot(ofp, resolution)

        msg = 'Screenshot {}'.format(ofn)
        Visum.WriteToLogFile(msg)
