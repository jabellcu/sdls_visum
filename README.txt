05/06/2017 Mon 17:24 JAC
This folder demonstrates the creation and automation of Spider Desire
Lines (SDLs) using VISUM and ArcMap. Inputs: 
  - zones.shp
  - demand.csv

Process steps:
   1) delaunay_shp.py (ArcMap): zones_shp ~> zones_links.shp
   2) CSV2FormatO.ipynb (jupyter notebook): demand.csv ~> *.mtx
   3) VISUM: import zones.shp
   4) VISUM: import zones_links.shp (use directed links)
   5) import_mtxs.py (VISUM) Use zone numbers (No)
   6) VISUM links: use standard values
   7) VISUM: create conectors
   8) VISUM conectors: use standard values
   9) VISUM turns: multi-edit open for all TSys
  10) VISUM > Calculate > Network check
  11) VISUM procedure sequence: Assign imported matrices / DSegs
  12) VISUM Display options: Display link bars
  13) VISUM: Save graphical parameters e.g. "SDLs.gpa"
  14) Ammend screenshots_from_gpa_links.py for "SDLs.gpa"
  15) screenshots_from_gpa_links.py (VISUM): automated screenshots.
