import os
import glob

sfiles = glob.glob('*.mtx')
mtn = 1
TSys = "C"

for sfp in sfiles:
    
    sfn, ext = os.path.splitext(sfp)
    
    # Impor matrix
    mt = Visum.Net.AddMatrix(mtn)
    mt.SetAttValue("Code", sfn)
    mt.SetAttValue("Name", sfn)
    mt.Open(sfp)

    msg = '{} imported.'.format(sfp)
    Visum.WriteToLogFile(msg)

    # Add DSeg
    DSeg = Visum.Net.AddDemandSegment(sfn, TSys)
    DSeg.ODMatrix = mt

    mtn += 1
